/* example-person.c
 *
 * Copyright 2021 tom <unknown@domain.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "example-person.h"
#include "example-person-private.h"

enum {
  PROP_0,
  PROP_NAME,
  PROP_AGE,
  N_PROPERTIES
};

enum {
  YO,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPERTIES];
static guint signals [N_SIGNALS];

G_DEFINE_TYPE (ExamplePerson, example_person, G_TYPE_OBJECT)

const gchar *
example_person_get_name (ExamplePerson *self)
{
  return self->name;
}

void
example_person_set_name (ExamplePerson *self,
                         const gchar   *name)
{
  if (g_strcmp0 (name, self->name) == 0) {
    // if the name is the same as current, do nothing
    g_info ("Trying to set the same name");
    
  } else {
    // if a new name is given, replace the old one
    g_free (self->name);
    self->name = g_strdup (name);
  }
}

guint
example_person_get_age (ExamplePerson *self)
{
  return self->age;
}

void
example_person_set_age (ExamplePerson *self,
                        guint          age)
{
  self->age = age;
}

static void
example_person_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  ExamplePerson *self = (ExamplePerson *)object;

  switch (prop_id) {
    case PROP_NAME:
      g_value_set_string (value, example_person_get_name (self));
      break;

    case PROP_AGE:
      g_value_set_uint (value, example_person_get_age (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
example_person_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  ExamplePerson *self = (ExamplePerson *)object;
  // ExamplePerson *self = EXAMPLE_PERSON (object); // same as above?

  switch (prop_id) {
    case PROP_NAME:
      example_person_set_name (self, g_value_get_string (value));
      break;

    case PROP_AGE:
      example_person_set_age (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

void
example_person_install_property (ExamplePerson   *object)
                                //  guint         prop_id,
                                //  const GValue *value,
                                //  GParamSpec   *pspec)
{
  ExamplePersonClass *klass = EXAMPLE_PERSON_GET_CLASS (object);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  // set up the new property
  GParamSpec *property = 
    g_param_spec_string ("name2",
                         "Name2",
                         "The name2 of the person",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  // install the property
  // TODO: should get current number of properties and increment 
  g_object_class_install_property (object_class, N_PROPERTIES, property);
}

static void
example_person_class_init (ExamplePersonClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  // override the object_class virtual methods for get/set
  object_class->get_property = example_person_get_property;
  object_class->set_property = example_person_set_property;

  properties [PROP_NAME] = 
    g_param_spec_string ("name",
                         "Name",
                         "The name of the person",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  
  properties [PROP_AGE] = 
    g_param_spec_uint ("age",
                       "Age",
                       "The age of the person",
                       10, 100, 20,
                       (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPERTIES, properties);

  signals [YO] =
    g_signal_new ("yo",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 0);
}

static void
example_person_init (ExamplePerson *self)
{
}




void
example_person_emit_yo (ExamplePerson *self)
{
  g_signal_emit (self, signals [YO], 0);
}

