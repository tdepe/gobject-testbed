#include <stdio.h>
#include "example-person.h"

int main()
{
    printf("Hello GObject\n");

    ExamplePerson *jane = NULL;
    jane = g_object_new(EXAMPLE_TYPE_PERSON, NULL);

    g_object_set(jane, "name", "Jane", NULL);
    g_object_set(jane, "age", 22, NULL);

    gchar *name = NULL;
    gchar *name2 = NULL;
    int age;

    g_object_get(jane, "name", &name, NULL);
    g_object_get(jane, "age", &age, NULL);

    g_print("My name is %s and I'm %i years old\n", name, age);

    // install a new property
    example_person_install_property(jane);

    g_object_get(jane, "name2", &name2, NULL);


    g_print("My name is %s and I'm %i years old\n", name, age);

    // clean up
    g_free (name);

    return 0;
}