#pragma once

#include "example-person.h"

G_BEGIN_DECLS

void         example_person_emit_yo  (ExamplePerson *self);

struct _ExamplePerson
{
  GObject  parent_instance;
  gchar   *name;
  guint    age;
};

G_END_DECLS