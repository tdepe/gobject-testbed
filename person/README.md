# Development Setup

Clone the repo. This module uses `meson` to configure builds and `ninja`
to compile. To compile the example do:

```sh
meson _build
ninja -C _build
```

Then run:
```
_build/person
```

# Guide

This guide is an attempt to provide a more pedagogical introduction to GObject than
the tutorial (https://developer.gnome.org/gobject/stable/howto-gobject.html).

The header file:
```c
/* example-person.h
 *
 * Copyright 2021 tom <unknown@domain.org>
 *
 * Put your license here
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define EXAMPLE_TYPE_PERSON (example_person_get_type())
G_DECLARE_FINAL_TYPE (ExamplePerson, example_person, EXAMPLE, PERSON, GObject)

// const gchar    *example_person_get_name                (ExamplePerson *self);
// void            example_person_set_name                (ExamplePerson *self,
//                                                         const gchar   *name);

// void            example_person_emit_yo                 (ExamplePerson *self);
void               example_person_install_property        (ExamplePerson *self);

#define EXAMPLE_PERSON_GET_CLASS(object) (G_TYPE_INSTANCE_GET_CLASS((object), EXAMPLE_TYPE_PERSON, ExamplePersonClass))

G_END_DECLS
```

`#pragma once` prevents you from importing the module more than once
`#include <glib-object.h>` includes the base GObejct library

The object declarations are contained between the macros `G_BEGIN_DECLS` and `G_END_DECLS`.
Perhaps the most interesting is the object declaration:

```c
#define EXAMPLE_TYPE_PERSON (example_person_get_type())
G_DECLARE_FINAL_TYPE (ExamplePerson, example_person, EXAMPLE, PERSON, GObject)
```
