/* example-person.h
 *
 * Copyright 2021 tom <unknown@domain.org>
 *
 * Put your license here
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define EXAMPLE_TYPE_PERSON (example_person_get_type())
// G_DECLARE_DERIVABLE_TYPE (ExamplePerson, example_person, EXAMPLE, PERSON, GObject)
G_DECLARE_FINAL_TYPE (ExamplePerson, example_person, EXAMPLE, PERSON, GObject)

// const gchar    *example_person_get_name                (ExamplePerson *self);
// void            example_person_set_name                (ExamplePerson *self,
//                                                         const gchar   *name);

// void            example_person_emit_yo                 (ExamplePerson *self);
void               example_person_install_property        (ExamplePerson *self);

#define EXAMPLE_PERSON_GET_CLASS(object) (G_TYPE_INSTANCE_GET_CLASS((object), EXAMPLE_TYPE_PERSON, ExamplePersonClass))

G_END_DECLS
